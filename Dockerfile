FROM ubuntu:focal


RUN source /etc/lsb-release && echo "deb https://download.rethinkdb.com/repository/ubuntu-$DISTRIB_CODENAME $DISTRIB_CODENAME main" | tee /etc/apt/sources.list.d/rethinkdb.list
RUN wget -qO- https://download.rethinkdb.com/repository/raw/pubkey.gpg | sudo apt-key add -
RUN apt-get update && apt-get install rethinkdb


WORKDIR /data

CMD ["rethinkdb", "--bind", "all"]