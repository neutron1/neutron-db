#!/bin/python3

import os
from rethinkdb import RethinkDB
from rethinkdb.errors import RqlRuntimeError, RqlDriverError

r = RethinkDB()

db_host = 'neutron-db'
db_port = '28015'
db_name = 'neutron'
table_list = ['state','config','plugins','things']
def db_create():
    connection = r.connect(host=db_host, port=db_port)
    try:
        r.db_create(db_name).run(connection)
        print("Database setup completed.")
    except RqlRuntimeError:
        print("Database already exists.")
    finally:
        connection.close()

def db_setup(table_list):
    connection = r.connect(host=db_host, port=db_port)
    try:
        for table in table_list:
            try:
                r.db(db_name).table_create(table).run(connection)
                print("Created table: " + table)
            except RqlRuntimeError:
                print("Table already exists: " + table)
    finally:
        connection.close()

db_create()
db_setup(table_list)
exit(0)